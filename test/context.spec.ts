import * as _ from 'lodash';

import { DataTransform, transform } from '../src/index';

import { TransformConfig } from 'src/transformconfig';

const data = {
  posts: [
    {
      title: 'title1',
    },
  ],
};

const map: TransformConfig = {
  list: 'posts',
  mapping: {
    greeting: 'title',
    farewell: '',
  },
};

describe('Transform Context', function () {
  it('should pass the context to operate.run', function () {
    const testConfig = _.clone(map);
    testConfig.operate = [
      {
        run: function customFn(item, _data, context) {
          return context.intro + item;
        },
        on: 'greeting',
      },
    ];

    const dataTransform = new DataTransform(_.clone(data), testConfig);

    const context = {
      intro: 'Hi ',
    };

    expect(dataTransform.transform(context)).toEqual([
      {
        greeting: 'Hi title1',
        farewell: '',
      },
    ]);
  });

  it('should pass the context to each', function () {
    const context = {
      intro: 'Hi ',
    };

    const testConfig = _.clone(map);
    testConfig.each = function customFn(item,_index, _data, context) {      
      item.greeting = context.intro + item.greeting
      return item;
    };

    const dataTransform = new DataTransform(_.clone(data), testConfig);

    expect(dataTransform.transform(context)).toEqual([
      {
        greeting: 'Hi title1',
        farewell: '',
      },
    ]);
  });

  it('should pass the context to each via functional api', function () {
    const testConfig = _.clone(map);
    testConfig.operate = [
      {
        run: function customFn(item, _data, context) {
          return context.intro + item;
        },
        on: 'greeting',
      },
      {
        run: `(val, data, context) => {                
                return context.outro
            }`,
        on: 'farewell',
      },
    ];

    const context = {
      intro: 'Hi ',
      outro: 'goodbye',
    };

    expect(transform(_.clone(data), testConfig, context)).toEqual([
      {
        greeting: 'Hi title1',
        farewell: 'goodbye',
      },
    ]);
  });
});
