import * as _ from 'lodash';

import { DataTransform, transform } from '../src/index';

import { TransformConfig } from 'src/transformconfig';

const data = {
  title: 'title1',
  description: 'description1',
  blog: 'This is a blog.',
  date: '04 Nov 2013',
  clearMe: 'text to remove',
  number: 23,
  extra: {
    link: 'https://goo.cm',
  },
  list1: [
    {
      name: 'mike',
    },
  ],
  list2: [
    {
      item: 'thing',
    },
  ],
};

const map: TransformConfig = {
  mapping: {
    name: 'title',
    info: 'description',
    text: 'blog',
    date: 'date',
    link: 'extra.link',
  },
  operate: [
    {
      run: 'Date.parse',
      on: 'date',
    },
    {
      run: function customFn(item) {
        if ('string' === typeof item) return item.toUpperCase();
        return item.toString().toUpperCase();
      },
      on: 'name',
    },
  ],
};

describe('Tranformation Operations', function () {
  it('should execute operations', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: 'Date.parse',
        on: 'date',
      },
      {
        run: function customFn(item) {
          if ('string' === typeof item) return item.toUpperCase();
          return item.toString().toUpperCase();
        },
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'TITLE1',
      info: 'description1',
      text: 'This is a blog.',
      date: Date.parse('11/4/2013'),
      link: 'https://goo.cm',
    });
  });

  it('should not execute operations when disabled', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: 'Date.parse',
        on: 'date',
      },
      {
        run: function customFn(item) {
          if ('string' === typeof item) return item.toUpperCase();
          return item.toString().toUpperCase();
        },
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap,{allowOperations:false});

    expect(dataTransform.transform()).toEqual({
      name: 'title1',
      info: 'description1',
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it('should execute simple stringfunction', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: '(val) => Date.parse(val)',
        on: 'date',
      },
      {
        run: '(val) => val.toUpperCase()',
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'TITLE1',
      info: 'description1',
      text: 'This is a blog.',
      date: Date.parse('11/4/2013'),
      link: 'https://goo.cm',
    });
  });

  it('should execute complex stringfunction', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: '(val) => Date.parse(val)',
        on: 'date',
      },
      {
        run: `(val) => {                
                return val.toUpperCase()                
            }`,
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'TITLE1',
      info: 'description1',
      text: 'This is a blog.',
      date: Date.parse('11/4/2013'),
      link: 'https://goo.cm',
    });
  });

  it('should execute operations with both item and input data', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: '(val) => Date.parse(val)',
        on: 'date',
      },
      {
        run: `(val, data) => {                
                return val.toUpperCase() + data.description
            }`,
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'TITLE1description1',
      info: 'description1',
      text: 'This is a blog.',
      date: Date.parse('11/4/2013'),
      link: 'https://goo.cm',
    });
  });

  it('should execute operations on unkown keys', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: `(val, data) => {
                
                return data.description
            }`,
        on: 'fakekey',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'title1',
      info: 'description1',
      fakekey: 'description1',
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it('should expose context to operations', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: `(val, data, context) => {                
                return context.key
            }`,
        on: 'fakekey',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform({ key: 'testvalue' })).toEqual({
      name: 'title1',
      info: 'description1',
      fakekey: 'testvalue',
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it('should sandbox operations', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: `(val, data, context) => {
                
                return process.env
            }`,
        on: 'fakekey',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform({ key: 'testvalue' })).toEqual({
      name: 'title1',
      info: 'description1',
      fakekey: {}, // env variables is empty
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it.todo('operators cannot mutate input values');
  it.todo('operators throw are handled');

  it.skip('should execute throw on function error', function () {
    const testMap = _.clone(map);
    testMap.operate = [
      {
        run: '(val) => Date.parse(val)',
        on: 'date',
      },
      {
        run: `(val) => {                
                return val.toUpperCase()                
            }`,
        on: 'name',
      },
    ];
    const dataTransform = new DataTransform(_.clone(data), testMap);

    expect(dataTransform.transform()).toEqual({
      name: 'TITLE1',
      info: 'description1',
      text: 'This is a blog.',
      date: Date.parse('11/4/2013'),
      link: 'https://goo.cm',
    });
  });

  it.skip('should transform data asynchronously', function () {
    const dataTransform = new DataTransform(_.clone(data), map);
    dataTransform.transformAsync().then(function (result) {
      expect(result).toEqual({
        name: 'TITLE1',
        info: 'description1',
        text: 'This is a blog.',
        date: Date.parse('11/4/2013'),
        link: 'https://goo.cm',
      });
    });
  });

  it('should allow you to use custom functions as operators', function () {
    const newMap = _.clone(map);

    newMap.operate = [
      {
        run: function (val) {
          return val + ' more info';
        },
        on: 'info',
      },
    ];

    const dataTransform = new DataTransform(data, newMap);

    const result = dataTransform.transform();
    expect(result).toEqual({
      name: 'title1',
      info: 'description1 more info',
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it('should allow you to use custom classes as operators', function () {
    const newMap = _.clone(map);

    class Operator {
      on = 'info';
      run = function (val): any {
        return val + ' more info';
      };
    }

    newMap.operate = [
      new Operator()
    ];

    const dataTransform = new DataTransform(data, newMap);

    const result = dataTransform.transform();
    expect(result).toEqual({
      name: 'title1',
      info: 'description1 more info',
      text: 'This is a blog.',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });

  it('should allow multiple operators', function () {
    const testMap = _.clone(map);

    testMap.operate = [
      {
        run: '(val) => val + " more info"',
        on: 'info',
      },
      {
        run: `(val) => val + ' more text'`,
        on: 'text',
      },
    ];

    const dataTransform = new DataTransform(data, testMap);

    const result = dataTransform.transform();
    expect(result).toEqual({
      name: 'title1',
      info: 'description1 more info',
      text: 'This is a blog. more text',
      date: '04 Nov 2013',
      link: 'https://goo.cm',
    });
  });
});
