

export class TransformConfig {
    list?: string;
    mapping?: any;
    remove?: string[];
    defaults?: any;
    operate?: OperationOption[]
    each?: string | EachFunction
}

export class OperationOption {
    run : string | OperationFunction;    
    on : string;
}

export interface OperationFunction {
    (val?: any, data?:any, context?:any )
}

export interface EachFunction {
    (item?:any,index?:number,data?:any,context?:any) 
}