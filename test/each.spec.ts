import * as _ from 'lodash';

import { DataTransform, transform } from '../src/index';

import { TransformConfig } from 'src/transformconfig';

const data = {
  title: 'title1',
  description: 'description1',
  blog: 'This is a blog.',
  date: '04 Nov 2013',
  clearMe: 'text to remove',
  number: 23,
  extra: {
    link: 'https://goo.cm',
  },
  list1: [
    {
      name: 'mike',
    },
  ],
  list2: [
    {
      item: 'thing',
    },
  ],
};

const map: TransformConfig = {
  mapping: {
    name: 'title',
    info: 'description',
    text: 'blog',
    date: 'date',
    link: 'extra.link',
  },
  operate: [
    {
      run: 'Date.parse',
      on: 'date',
    },
    {
      run: function customFn(item) {
        if ('string' === typeof item) return item.toUpperCase();
        return item.toString().toUpperCase();
      },
      on: 'name',
    },
  ],
};

describe('Tranformation Each', function () {
  it('should allow each function to run on all items', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      each: function (item) {
        item.iterated = true;
        return item;
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();
    expect(result).toEqual([
      { name: 'peter', iterated: true },
      { name: 'paul', iterated: true },
      { name: 'marry', iterated: true },
    ]);
  });

  it('should not run each function when disabled via config', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      each: function (item) {
        item.iterated = true;
        return item;
      },
    };

    const dataTransform = new DataTransform(data, map, { allowEach: false });

    const result = dataTransform.transform();
    expect(result).toEqual([{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }]);
  });

  it('should allow each function to be strings', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      each: '(item) => item.iterated = true',
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();
    expect(result).toEqual([
      { name: 'peter', iterated: true },
      { name: 'paul', iterated: true },
      { name: 'marry', iterated: true },
    ]);
  });

  it('should not allow each function to be strings when disabled', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      each: '(item) => item.iterated = true',
    };

    const dataTransform = new DataTransform(data, map, { allowStringFunctions: false });

    const result = dataTransform.transform();
    expect(result).toEqual([{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }]);
  });

  it('should allow each function see data and context', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      each: function (item, index, data, context) {
        item.index = index;
        item.size = data.length;
        item.iterated = true;
        item.contextKey = context.contextKey;
        return item;
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform({ contextKey: 'contextValue' });
    expect(result).toEqual([
      { name: 'peter', iterated: true, index: 0, size: 3, contextKey: 'contextValue' },
      { name: 'paul', iterated: true, index: 1, size: 3, contextKey: 'contextValue' },
      { name: 'marry', iterated: true, index: 2, size: 3, contextKey: 'contextValue' },
    ]);
  });

  it('should be able to combine mapping with each', function () {
    const data = {
      posts: [{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      mapping: {
        title: 'name',
      },
      each: function (item) {
        item.iterated = true;
        return item;
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();
    expect(result).toEqual([
      { title: 'peter', iterated: true },
      { title: 'paul', iterated: true },
      { title: 'marry', iterated: true },
    ]);
  });
});
