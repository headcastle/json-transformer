import type { Config } from '@jest/types';

const reportDir = './report';
const reportName = `report-${Math.floor(new Date().getTime() / 1000)}.html`;

const config: Config.InitialOptions = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  // rootDir: 'test',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: ['src/**/*.(t|j)s'],
  coverageReporters: ['html', ['lcov', { projectRoot: '__dirname' }], 'text', 'text-summary', 'cobertura'],
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  reporters: [
    'default',
    'jest-junit',
    [
      'jest-html-reporters',
      {
        publicPath: reportDir,
        filename: reportName,
        // expand: true,
        // openReport: true,
      },
    ],
  ],
};

export default config;
