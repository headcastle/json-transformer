import * as _ from 'lodash';

import { NodeVM } from 'vm2';
import { TransformConfig } from './transformconfig';
import { TransformOptions } from './transformoptions';

export class DataTransform {
  private readonly data;
  private readonly map: TransformConfig;
  private readonly options: TransformOptions;
  private readonly vm;

  constructor(inputData: any, transformConfig: TransformConfig, transformOption?: TransformOptions) {
    this.data = inputData;
    this.map = transformConfig;
    this.options = _.defaults(transformOption, new TransformOptions());
    this.vm = new NodeVM({
      // wrapper: 'none'
    });
  }

  applyDefault(key: string): any {
    return key && this.map.defaults ? this.map.defaults[key] : undefined;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  getValue(obj: any, key?: string, newKey?: string): any {
    if (typeof obj === 'undefined') {
      return;
    }

    if (key == '' || key == undefined) {
      return obj;
    }

    const value = obj || this.data;

    key = key || this.map.list;
    return _.get(value, key, this.applyDefault(newKey));
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  setValue(obj: any, key: string | undefined, newValue: any): void {
    if (typeof obj === 'undefined') {
      return;
    }

    if (key == '' || key == undefined) {
      return;
    }

    const keys = key.split('.');
    let target = obj;
    for (let i = 0; i < keys.length; i++) {
      if (i === keys.length - 1) {
        target[keys[i]] = newValue;
        return;
      }
      if (keys[i] in target) target = target[keys[i]];
      else return;
    }
  }

  getList(): any {
    return this.getValue(this.data, this.map.list);
  }

  transformOld(context?: any): any {
    const useList = this.map.list != undefined;
    let value;
    if (useList) {
      value = this.getValue(this.data, this.map.list);
    } else if (_.isArray(this.data) && !useList) {
      value = this.data;
    } else if (_.isObject(this.data) && !useList) {
      value = [this.data];
    }
    let normalized = [];

    if (!_.isEmpty(value)) {
      const list = useList ? this.getList() : value;
      normalized = this.map.mapping ? _.map(list, _.bind(this.iterator, this, this.map.mapping)) : list;
      normalized = _.bind(this.operate, this, normalized)(context);
      normalized = this.each(normalized, context);
      normalized = this.removeAll(normalized);
    }

    if (!useList && _.isObject(this.data) && !_.isArray(this.data)) {
      return normalized[0];
    }

    return normalized;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  transform(context?: any): any {
    const useList = this.map.list != undefined;
    let value;
    if (useList) {
      value = this.getValue(this.data, this.map.list);
    } else if (_.isArray(this.data)) {
      value = this.data;
    } else if (_.isObject(this.data)) {
      value = [this.data];
    }
    let normalized = [];

    if (!_.isEmpty(value)) {
      const list = useList ? this.getList() : value;
      normalized = this.map.mapping ? _.map(list, _.bind(this.iterator, this, this.map.mapping)) : list;
      if (this.options.allowOperations) {
        normalized = this.operate(normalized, context);
      }
      if (this.options.allowEach) {
        normalized = this.each(normalized, context);
      }
      normalized = this.removeAll(normalized);
    } else {
      return value;
    }

    if (!useList && _.isObject(this.data) && !_.isArray(this.data)) {
      return normalized[0];
    }

    return normalized;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  transformAsync(context?: any): Promise<any> {
    return new Promise(
      function (resolve, reject) {
        try {
          resolve(this.transform(context));
        } catch (err) {
          reject(err);
        }
      }.bind(this),
    );
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  removeAll(data: any): any {
    if (_.isArray(this.map.remove)) {
      return _.each(data, (item) => this.remove(item));
    }
    return data;
  }

  remove(item) {
    _.each(this.map.remove, (key) => {
      delete item[key];
    });
    return item;
  }

  operate(data, context) {
    if (this.map.operate) {
      _.each(this.map.operate, (method) => {
        data = _.map(data, (item) => {
          let fn;
          if ('string' === typeof method.run) {
            fn = this.vm.run(`module.exports = ${method.run}`);
          } else {
            fn = method.run;
          }
          this.setValue(item, method.on, fn(this.getValue(item, method.on), this.data, context));
          return item;
        });
      });
    }
    return data;
  }

  each(data, context): any {
    if (this.map.each) {
      let fn;
      if ('string' === typeof this.map.each && this.options.allowStringFunctions) {
        fn = this.vm.run(`module.exports = ${this.map.each}`);
      } else if ('string' !== typeof this.map.each) {
        fn = this.map.each;
      }
      if (fn) {
        _.forEach(data, (value, index, collection) => {
          return fn(value, index, collection, context);
        });
      }
    }
    return data;
  }

  iterator(map, item) {
    const obj = {};

    //to support simple arrays with recursion
    if (typeof map === 'string') {
      return this.getValue(item, map);
    }
    _.each(
      map,
      _.bind(function (oldkey, newkey) {
        if (typeof oldkey === 'string' && oldkey.length > 0) {
          const value = this.getValue(item, oldkey, newkey);
          if (value !== undefined) obj[newkey] = value;
        } else if (_.isArray(oldkey)) {
          const array = _.map(
            oldkey,
            _.bind(
              function (item, map) {
                return this.iterator(map, item);
              },
              this,
              item,
            ),
          ); //need to swap arguments for bind
          obj[newkey] = array;
        } else if (typeof oldkey === 'object') {
          obj[newkey] = this.iterator(oldkey, item);
        } else {
          obj[newkey] = '';
        }
      }, this),
    );
    return obj;
  }
}
