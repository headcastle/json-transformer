import { TransformOptions } from '../src/transformoptions';

describe('transformOptions', function () {
  it('should be defined', function () {
    const transformOptions = new TransformOptions();

    expect(transformOptions).toBeDefined();
  });
});
