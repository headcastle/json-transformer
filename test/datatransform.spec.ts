import * as _ from 'lodash';

import { DataTransform } from '../src/datatransform';
import { TransformConfig } from 'src/transformconfig';

const data = {
  posts: [
    {
      title: 'title1',
      description: 'description1',
      blog: 'This is a blog.',
      date: '11/4/2013',
      clearMe: 'text to remove',
      extra: {
        link: 'http://goo.cm',
      },
      list1: [
        {
          name: 'mike',
        },
      ],
      list2: [
        {
          item: 'thing',
        },
      ],
    },
  ],
  simpleObject: {
    info: 'more inforations',
  },
  simpleKey: 'testkey',
};

const map : TransformConfig = {
  list: 'posts',
  mapping: {
    name: 'title',
    info: 'description',
    text: 'blog',
    date: 'date',
    link: 'extra.link',
  },
  operate: [
    {
      run: 'Date.parse',
      on: 'date',
    },
    {
      run: function customFn(item) {
        if ('string' === typeof item) return item.toUpperCase();
        return item.toString().toUpperCase();
      },
      on: 'name',
    },
  ],
};

describe('DataTransform Class', () => {
  describe('applyDefault method', () => {
    it('should provide Default by Key', () => {
      const testMap = _.clone(map);
      testMap.defaults = {
        missingData: true,
      };
      const dataTransform = new DataTransform(_.clone(data), testMap);

      expect(dataTransform.applyDefault('missingData')).toBe(true);
    });
    it('should provide complex object for Default', () => {
      const testMap = _.clone(map);
      testMap.defaults = {
        missingData: {
          info: 'more inforations',
        },
      };
      const dataTransform = new DataTransform(_.clone(data), testMap);

      expect(dataTransform.applyDefault('missingData')).toStrictEqual(testMap.defaults.missingData);
    });

    it('should extract undefined for without Default', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.applyDefault('fakekey')).toBeUndefined();
    });

    it('should extract undefined for without Default by Key', () => {
      const testMap = _.clone(map);
      testMap.defaults = {
        missingData: true,
      };
      const dataTransform = new DataTransform(_.clone(data), testMap);

      expect(dataTransform.applyDefault('fakekey')).toBeUndefined();
    });
  });

  describe('getValue method', () => {
    it('should extract values', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(data, 'simpleKey')).toEqual('testkey');
    });

    it('should extract values from constructor', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(false, 'simpleKey')).toEqual('testkey');
      expect(dataTransform.getValue(0, 'simpleKey')).toEqual('testkey');
      expect(dataTransform.getValue(null, 'simpleKey')).toEqual('testkey');
    });

    it('should extract deep values', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(data, 'posts.0.description')).toEqual('description1');
    });

    it('should extract undefined for nonexistant keys', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(data, 'fakekey')).toBeUndefined();
    });

    it('should extract object for empty keys', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(data, '')).toBe(data);
    });

    it('should extract object for undefined keys', () => {
      const dataTransform = new DataTransform(_.clone(data), map);

      expect(dataTransform.getValue(data, undefined)).toBe(data);
      expect(dataTransform.getValue(data)).toBe(data);
    });

    it('should extract undefined for any key on undefined object', () => {
      const dataTransform = new DataTransform(undefined, map);

      expect(dataTransform.getValue(undefined, 'fakekey')).toBeUndefined();
    });

    it.todo('extra data from array by List variable');
  });

  describe('setValue method', () => {
    it.todo('should be tested');
  });
});
