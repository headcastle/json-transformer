export class TransformOptions {
  exposeConsole? = false;
  allowStringFunctions? = true;
  allowOperations? = true;
  allowEach? = true;
}
