import * as _ from 'lodash';

import { TransformConfig } from 'src/transformconfig';
import { transform } from '../src/index';

const data = [
  {
    title: 'title1',
  },
];

const map: TransformConfig = {
  mapping: {
    name: 'title',
  },
};

describe('node-json-transform', function () {
  it('should not manipulate the raw data', function () {
    const clone = _.clone(data);

    transform(data, map);

    expect(clone).toEqual(data);
  });

  it('should not manipulate the raw data', function () {
    const clone = _.clone(map);

    transform(data, map);

    expect(clone).toEqual(map);
  });
});
