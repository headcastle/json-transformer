import * as _ from 'lodash';

import { DataTransform, transform } from '../src/index';

import { TransformConfig } from 'src/transformconfig';

const data = {
  posts: [
    {
      title: 'title1',
      description: 'description1',
      blog: 'This is a blog.',
      date: '11/4/2013',
      clearMe: 'text to remove',
      extra: {
        link: 'https://goo.cm',
      },
      list1: [
        {
          name: 'mike',
        },
      ],
      list2: [
        {
          item: 'thing',
        },
      ],
    },
  ],
};

const map: TransformConfig = {
  list: 'posts',
  mapping: {
    name: 'title',
    info: 'description',
    text: 'blog',
    date: 'date',
    link: 'extra.link',
  },
  operate: [
    {
      run: 'Date.parse',
      on: 'date',
    },
    {
      run: function customFn(item) {
        if ('string' === typeof item) return item.toUpperCase();
        return item.toString().toUpperCase();
      },
      on: 'name',
    },
  ],
};

describe('transform', function () {
  it('should transform data', function () {
    const dataTransform = new DataTransform(_.clone(data), map);

    expect(dataTransform.transform()).toEqual([
      {
        name: 'TITLE1',
        info: 'description1',
        text: 'This is a blog.',
        date: Date.parse('11/4/2013'),
        link: 'https://goo.cm',
      },
    ]);
  });

  it('should transform data asynchronously', function () {
    const dataTransform = new DataTransform(_.clone(data), map);
    dataTransform.transformAsync().then(function (result) {
      expect(result).toEqual([
        {
          name: 'TITLE1',
          info: 'description1',
          text: 'This is a blog.',
          date: Date.parse('11/4/2013'),
          link: 'https://goo.cm',
        },
      ]);
    });
  });

  it('should allow you to pass arrays without specifying a list', function () {
    // Add a map item to  clear out the "clearMe" field.
    const newMap = {
      mapping: {
        fieldGroup: ['title', 'description', 'blog', 'extra'],
      },
    };

    const data = [
      {
        title: 'title1',
        description: 'description1',
        blog: 'This is a blog.',
        date: '11/4/2013',
        clearMe: 'text to remove',
        extra: {
          link: 'https://goo.cm',
        },
        list1: [
          {
            name: 'mike',
          },
        ],
        list2: [
          {
            item: 'thing',
          },
        ],
      },
    ];

    const dataTransform = new DataTransform(_.clone(data), newMap);

    expect(dataTransform.transform()).toEqual([
      {
        fieldGroup: [
          'title1',
          'description1',
          'This is a blog.',
          {
            link: 'https://goo.cm',
          },
        ],
      },
    ]);
  });

  it('should delete attributes', function () {
    const data = {
      posts: [
        { name: 'peter', unwanted: true },
        { name: 'paul', unwanted: true },
        { name: 'marry', unwanted: true },
      ],
    };

    const map: TransformConfig = {
      list: 'posts',
      remove: ['unwanted'],
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();

    expect(result).toEqual([{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }]);
  });

  it('should use default attributes for missing data', function () {
    const data = {
      posts: [{ name: 'peter', valid: true }, { name: 'paul', valid: true }, { name: 'marry' }],
    };

    const map: TransformConfig = {
      list: 'posts',
      mapping: {
        verified: 'valid',
        name: 'name',
      },
      defaults: {
        verified: false,
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();

    expect(result).toEqual([
      { name: 'peter', verified: true },
      { name: 'paul', verified: true },
      { name: 'marry', verified: false },
    ]);
  });

  it('should transform an object literal if list is not set and data is an object', function () {
    const data = {
      text: 'hello',
    };

    const map = {
      mapping: {
        message: 'text',
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();

    expect(result).toEqual({
      message: 'hello',
    });
  });

  it('should transform via transform function', function () {
    const data = {
      text: 'hello',
    };

    const map: TransformConfig = {
      mapping: {
        message: 'text',
      },
    };

    const result = transform(data, map);

    expect(result).toEqual({
      message: 'hello',
    });
  });
});
