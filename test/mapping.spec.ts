import * as _ from 'lodash';

import { DataTransform, transform } from '../src/index';

import { TransformConfig } from 'src/transformconfig';

const data = {
  posts: [
    {
      title: 'title1',
      description: 'description1',
      blog: 'This is a blog.',
      date: '11/4/2013',
      clearMe: 'text to remove',
      extra: {
        link: 'https://goo.cm',
      },
      list1: [
        {
          name: 'mike',
        },
      ],
      list2: [
        {
          item: 'thing',
        },
      ],
    },
  ],
};

const map: TransformConfig = {
  list: 'posts',
  mapping: {
    name: 'title',
    info: 'description',
    text: 'blog',
    date: 'date',
    link: 'extra.link',
  },
  operate: [
    {
      run: 'Date.parse',
      on: 'date',
    },
    {
      run: function customFn(item) {
        if ('string' === typeof item) return item.toUpperCase();
        return item.toString().toUpperCase();
      },
      on: 'name',
    },
  ],
};

describe('transform mapping', function () {

  it('should allow you to clear out fields', function () {
    // Add a map item to  clear out the "clearMe" field.
    const newMap: TransformConfig = _.clone(map);
    newMap.mapping = _.clone(map.mapping);
    newMap.mapping.clearMe = ''; 

    const dataTransform = new DataTransform(_.clone(data), newMap);

    expect(dataTransform.transform()).toEqual([
      {
        name: 'TITLE1',
        info: 'description1',
        text: 'This is a blog.',
        date: Date.parse('11/4/2013'),
        link: 'https://goo.cm',
        clearMe: '',
      },
    ]);
  });

  it('should allow you to set fields', function () {
    // Add a map item to  clear out the "clearMe" field.
    const newMap: TransformConfig = _.clone(map);
    newMap.mapping = _.clone(map.mapping);
    newMap.mapping.fieldThatDoesntExist = '';

    const dataTransform = new DataTransform(_.clone(data), newMap);

    expect(dataTransform.transform()).toEqual([
      {
        name: 'TITLE1',
        text: 'This is a blog.',
        date: Date.parse('11/4/2013'),
        link: 'https://goo.cm',
        info: 'description1',
        fieldThatDoesntExist: '',
      },
    ]);
  });

  it('should allow you to map arrays', function () {
    // Add a map item to  clear out the "clearMe" field.
    const newMap: TransformConfig = {
      list: 'posts',
      mapping: {
        fieldGroup: ['title', 'description', 'blog', 'extra'],
      },
    };

    const dataTransform = new DataTransform(_.clone(data), newMap);

    expect(dataTransform.transform()).toEqual([
      {
        fieldGroup: [
          'title1',
          'description1',
          'This is a blog.',
          {
            link: 'https://goo.cm',
          },
        ],
      },
    ]);
  });

   it('should exclude data if not specified in mapping', function () {
    const data = {
      posts: [
        { name: 'peter', unwanted: true },
        { name: 'paul', unwanted: true },
        { name: 'marry', unwanted: true },
      ],
    };

    const map: TransformConfig = {
      list: 'posts',
      mapping: {
        name: 'name',
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();

    expect(result).toEqual([{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }]);
  });

  it('should allow for dots in object keys', function () {
    const data = {
      input: [{ key: { 'dot.key': 'peter' } }, { key: { 'dot.key': 'paul' } }, { key: { 'dot.key': 'marry' } }],
    };

    const map: TransformConfig = {
      list: 'input',
      mapping: {
        name: 'key["dot.key"]',
      },
    };

    const dataTransform = new DataTransform(data, map);

    const result = dataTransform.transform();

    expect(result).toEqual([{ name: 'peter' }, { name: 'paul' }, { name: 'marry' }]);
  });

  it('should try use the map directly if no item', function () {
    const data = {
      text: 'hello',
    };

    const map: TransformConfig = {
      mapping: { message: 'text' },
    };

    const result = transform(data, map);

    expect(result).toEqual({
      message: 'hello',
    });
  });

  it('should omit undefined keys from the result', function () {
    const data = [
      {
        text: 'hello',
      },
      {},
    ];

    const map: TransformConfig = {
      mapping: { message: 'text' },
    };

    const result = transform(data, map);

    expect(result).toEqual([
      {
        message: 'hello',
      },
      {},
    ]);
  });
});
