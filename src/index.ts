import { DataTransform } from './datatransform';
import { TransformConfig } from './transformconfig';

export * from './datatransform';
export * from './transformoptions';
export * from './transformconfig';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const transform = function (data: any, map: TransformConfig, context?: any): any {
  const dataTransform = new DataTransform(data, map);
  return dataTransform.transform(context);
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const transformAsync = function (data: any, map, context?: any): any {
  const dataTransform = new DataTransform(data, map);
  return dataTransform.transformAsync(context);
};
