import { TransformConfig } from '../src/transformconfig';

describe('TransformConfig', function () {
  it('should be defined', function () {
    const transformConfig = new TransformConfig();

    expect(transformConfig).toBeDefined();
  });
  
});
